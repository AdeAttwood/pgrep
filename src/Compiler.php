<?php

namespace pgrep;

use Symfony\Component\Finder\Finder;

class Compiler
{
    public $name = 'pgrep.phar';

    private $version;
    private $branchAliasVersion = '';
    private $versionDate;

    private $basePath;

    public function __construct()
    {
        $this->basePath = dirname(__DIR__).'/';
    }

    public function getFinder()
    {
        $finder = new Finder();

        $finder->files()
            ->ignoreVCS(true)
            ->name('*.php')
            ->notName('Compiler.php')
            ->exclude('Tests')
            ->exclude('tests')
            ->exclude('docs')
            ->in($this->basePath.'src')
            ->in($this->basePath.'bin')
            ->in($this->basePath.'vendor/symfony')
            ->in($this->basePath.'vendor/composer');

        return $finder;

    }

    public function getFiles()
    {
        return [
            $this->basePath.'bin/pgrep',
            $this->basePath.'vendor/autoload.php'
        ];
    }

    public function compile()
    {
        echo "Starting\n";

        $phar = new \Phar($this->name, 0, $this->name);
        $phar->setSignatureAlgorithm(\Phar::SHA1);
        $phar->startBuffering();

        foreach ($this->getFinder() as $file) {
            $this->addFile($phar, $file);
        }

        foreach ($this->getFiles() as $file) {
            $this->addFile($phar, $file);
        }

        $phar->setStub(<<<EOL
#!/usr/bin/env php
<?php
Phar::mapPhar();
require 'phar://{$this->name}/bin/pgrep';
__HALT_COMPILER();      
EOL
        );
        

        $phar->stopBuffering();
    }

    public function addFile($phar, $file)
    {
        if (is_string($file)) {
            $file = new \SplFileInfo($file);
        }
        
        $path = str_replace($this->basePath, '', $file->getRealPath());

        echo "Adding {$path}\n";

        $content = file_get_contents($file->getRealPath());
        $content = preg_replace('{^#!/usr/bin/env php\s*}', '', $content);
        $content = $this->stripWhitespace($content);

        $phar->addFromString($path, $content);
    }

    /**
     * Removes whitespace from a PHP source string while preserving line numbers.
     *
     * @param string $source A PHP string
     * 
     * @return string The PHP string with the whitespace removed
     */
    public function stripWhitespace($source)
    {
        if (!function_exists('token_get_all')) {
            return $source;
        }

        $output = '';

        foreach (token_get_all($source) as $token) {
            if (is_string($token)) {
                $output .= $token;
            } elseif (in_array($token[0], array(T_COMMENT, T_DOC_COMMENT))) {
                $output .= str_repeat("\n", substr_count($token[1], "\n"));
            } elseif (T_WHITESPACE === $token[0]) {
                // reduce wide spaces
                $whitespace = preg_replace('{[ \t]+}', ' ', $token[1]);
                // normalize newlines to \n
                $whitespace = preg_replace('{(?:\r\n|\r|\n)}', "\n", $whitespace);
                // trim leading spaces
                $whitespace = preg_replace('{\n +}', "\n", $whitespace);
                $output .= $whitespace;
            } else {
                $output .= $token[1];
            }
        }

        return $output;
    }

}