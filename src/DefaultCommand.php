<?php

namespace pgrep;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Exception;

class DefaultCommand extends \Symfony\Component\Console\Command\Command
{

    public $count = 0;

    public $outputTemplate = '{file}:{lineNumber} {line}';

    /**
     * Sets the name, help, options and arguments for the command
     *
     * @return void
     */
    protected function configure()
    {
        $options = [
            [
                'case-insensitive',
                'i',
                InputOption::VALUE_NONE,
                'Case insensitive regex'
            ],
            [
                'count',
                null,
                InputOption::VALUE_NONE,
                'Only print the total number of matches found'
            ],
            [
                'in',
                null,
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'Directory to find files in',
                [getcwd()]
            ],
            [
                'size',
                null,
                InputOption::VALUE_OPTIONAL,
                'A size range of files to look into'
            ],
            [
                'date',
                null,
                InputOption::VALUE_OPTIONAL,
                'Date range to search in files. The date must be something that strtotime()'
            ],
            [
                'depth',
                null,
                InputOption::VALUE_OPTIONAL,
                'The depth of directors to dive into'
            ],
        ];

        $this->setName('pgrep')
            ->setDescription('Grep with php regex')
            ->addArgument('pattern', InputArgument::REQUIRED, 'The pattern to look for')
            ->setHelp('Search for php regex in files');

        foreach ($options as $option) {
            call_user_func_array([$this, 'addOption'], $option);
        }
    }

    /**
     * Runs the command
     *
     * @param InputInterface  $input  The interface implemented the symfony input class
     * @param OutputInterface $output The interface implemented the symfony output class
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputStream = $input->getStream() ?: STDIN;
        stream_set_blocking($inputStream, 0);
        $pipedInput = stream_get_contents($inputStream);

        $pattern = '/'.$input->getArgument('pattern').'/';

        if ($input->getOption('case-insensitive') === true) {
            $pattern .= 'i';
        }

        if ($pipedInput !== '') {
            $this->outputTemplate = '{line}';
            $out = $this->grepContents($pattern, $pipedInput);

            if (!$input->getOption('count')) {
                $output->writeln($out);
            } else {
                $output->writeln($this->count);
            }

            return;
        }

        foreach ($this->buildFinder($input) as $file) {
            try {
                $contents = $file->getContents();
            } catch (Exception $e) {
                continue;
            }

            $out = $this->grepContents($pattern, $contents, $file);

            if (!$input->getOption('count')) {
                $output->writeln($out);
            }
        }

        if ($input->getOption('count')) {
            $output->writeln($this->count);
        }
    }

    public function grepContents($pattern, $contents, $file = null)
    {
        $lines = explode("\n", $contents);

        preg_match_all($pattern, $contents, $matches, PREG_OFFSET_CAPTURE);

        $out = [];

        foreach (current($matches) as $match) {
            $this->count++;

            $lineIndex = substr_count(mb_substr($contents, 0, $match[1]), PHP_EOL);
            $line = preg_replace($pattern, '<fg=green;options=bold>$0</>', $lines[$lineIndex]);

            $out[] = strtr($this->outputTemplate, [
                '{matchValue}' => $match[0],
                '{file}' => $file ? $file->getPathName() : '',
                '{fileRelative}' => $file ? $file->getRelativePathname() : '',
                '{line}' => $line,
                '{lineNumber}' => $lineIndex + 1,
            ]);
        }

        return implode(PHP_EOL, $out);
    }

    public function buildFinder($input)
    {
        $finder = new Finder();
        $finder->ignoreUnreadableDirs()->files();

        foreach ($input->getOption('in') as $dir) {
            $finder->in($dir);
        }

        if (($size = $input->getOption('size')) !== null) {
            $finder->size($size);
        }

        if (($date = $input->getOption('date')) !== null) {
            $finder->date($date);
        }

        if (($depth = $input->getOption('depth')) !== null) {
            $finder->depth($depth);
        }

        return $finder;
    }
}
