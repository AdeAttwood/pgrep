<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    // define public methods as commands

    /**
     * @command my-project:command-one
     */
    function hello()
    {
        $this->io()->title("Build all site assets");
        $this->taskExec('ls')->run();
    }
    
}