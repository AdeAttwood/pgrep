<?php


class CoreTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $app;

    protected function _before()
    {


    }

    protected function _after()
    {
    }

    /**
     * Test the count feature works
     *
     * @return void
     */
    public function testCount()
    {
        $commandTester = $this->tester->createTestCommand();
        $commandTester->execute([
            'pattern' => 'Lorem ipsum',
            '--in' => ['tests/_data'],
            '--count' => true,
        ]);

        $this->tester->assertEquals(6, (integer)$commandTester->getDisplay());
    }

    /**
     * Test the case-insensitive
     *
     * @return void
     */
    public function testCaseInsensitive()
    {
        $commandTester1 = $this->tester->createTestCommand();
        $commandTester1->execute([
            'pattern' => 'sed',
            '--in' => ['tests/_data'],
            '--count' => true,
        ]);

        $this->tester->assertEquals(6, (integer)$commandTester1->getDisplay());

        $commandTester2 = $this->tester->createTestCommand();
        $commandTester2->execute([
            'pattern' => 'sed',
            '--in' => ['tests/_data'],
            '--count' => true,
            '-i' => true
        ]);

        $this->tester->assertEquals(8, (integer)$commandTester2->getDisplay());
    }
}