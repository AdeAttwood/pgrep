<?php
// This is global bootstrap for autoloading
//
error_reporting(E_ALL);

if (is_file(__DIR__.'/../vendor/autoload.php')) {
    require_once __DIR__.'/../vendor/autoload.php';
} else {
    require_once __DIR__.'/../../../autoload.php';
}
