<?php

set_time_limit(0);

$autoLoaders = [
    __DIR__.'/../vendor/autoload.php',
    __DIR__.'/../../../autoload.php'
];

foreach ($autoLoaders as $loader) {
    if (is_file($loader)) {
        include_once $loader;
    }
}